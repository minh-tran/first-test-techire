1. Images Creation screen
	Link: /images/add
	Validation rules:
		+ Size of Image width: 1 - 5000 (px).
		+ Size of Image height: 1 - 5000 (px).
		+ Top of Image: 0 - 1000 (px).
		+ Left of Image: 0 - 1000 (px).
		+ Maximum file size: 3 (MB).
		+ files extension: 'jpeg', 'png', 'jpg'

2. Images List screen
	Link: /images
	CUSTOM SEARCH CONDITION rules:
		+ Before or after OPERATOR has to a spacing.
			Exps:
				filename CONTRAIN bubble gum (correct).
				filename CONTRAINbubble gum (incorrect).
				imgw < 100 (correct).
				imgw <100 (incorrect).
		+ keywords or operators are value.
			Exps:
				filename CONTRAIN bubble gum (correct).
				filename CONTRAIN AND imgw  (incorrect).
				filename CONTRAIN imgw AND  (incorrect).
				filename CONTRAIN = (incorrect).
				filename CONTRAIN < (incorrect).
				filename CONTRAIN > (incorrect).
				filename CONTRAIN ( (incorrect).
				etc.

My note:
The CUSTOM SEARCH CONDITION structure does not work for every cases. Because i have to change the structure of the CONDITION.
exps:
	sql 01: 
		filename CONTRAIN bubble gum AND (imgw > 100 OR imgh > 200)
	Structure: 
	[
		'filename LIKE' => '%bubble gum%',
		'OR' => [
			'imgw >' => 100
			'imgh >' => 200
		]
	]
	Results: correct
	
	sql 02: 
		filename CONTRAIN bubble gum AND (imgw > 100 OR imgh > 100) AND (imgw < 200 OR imgh < 200)(correct)
	Structure: 
	[
		'filename LIKE' => '%bubble gum%',
		'OR' => [
			'imgw >' => 100
			'imgh >' => 100
		],
		'OR' => [
			'imgw <' => 200
			'imgh <' => 200
		]
	]
	Results: incorrect
	Reasons: Dupplicate key [OR] of array. There are many cases incorrect.
	Solutions:
	[
		[
			'filename LIKE' => '%bubble gum%'
		],
		[
			'OR' => [
				'imgw >' => 100
				'imgh >' => 100
			]
		],
		[
			'OR' => [
				'imgw <' => 200
				'imgh <' => 200
			]
		]
	]
	

				
				
			
		
