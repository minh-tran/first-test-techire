<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\PaginatorHelper;
use App\Form\ImageForm;

/**
 * Static content controller
 *
 * This controller will render views from Template/Images/
 *
 */
class ImagesController extends AppController
{

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Images.filename' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        // $this->loadComponent('Paginator', ['templates' => 'paginator-templates']);
    }

    /**
     * Images Lists.
     *
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function index()
    {
        // Set the layout.
        $this->viewBuilder()->setLayout('default');

        $urlParams = $this->request['url'];
        $filter = null;
        $images = TableRegistry::get('images');

        if (empty($urlParams)) {
            $query = $images->find();
        } elseif (isset($urlParams['page']) || isset($urlParams['filter'])) {
            $page = null;

            // get pagination.
            if (isset($urlParams['page'])) {
                $page = $urlParams['page'];
            }

            // get filter.
            if (isset($urlParams['filter'])) {
                $filter = $urlParams['filter'];
            }

            $condition = $this->parseCondition($filter);

            $validate = [];
            $this->validateFields($condition, $validate);

            if (count($validate) == 0) {
                $query = $images
                    ->find()
                    ->where($condition);
            } else {
                // set error messages.
                $query = $images->find();

                foreach ($validate as $type => $types) {
                    foreach ($types as $key => $value) {
                        $operator = strtoupper($key);
                        $field = strtoupper($value);

                        if ($type == 'fields') {
                            $msg = 'Unknown field ' . $field;
                        } else {
                            $msg = 'Unknown operator ' . $operator . ' for field ' . $field;
                        }
                        $this->Flash->error($msg);
                    }
                }
            }
        }
        
        $this->set([
            'data' => $this->paginate($query),
            'filter' => $filter
        ]);
        $this->render();
    }

    private function parseCondition($string)
    {
        $string = strtolower($string);

        // parse by [brackets].
        $brackets = $this->parseBracket($string);


        // parse by [and] condition.
        $ands = [];
        foreach ($brackets as $key => $value) {
            $cond = $this->parseAnd($value);

            if (!empty($cond)) {
                $ands[] = $cond;
            }
        }

        // parse by [or] condition.
        $ors = [];
        foreach ($ands as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $cond = $this->parseOr($v);

                    if (!empty($cond)) {
                        $ors[] = $cond;
                    }
                }
            } else {
                $cond = $this->parseOr($value);

                if (!empty($cond)) {
                    $ors[] = $cond;
                }
            }
        }

        $result = $ors;
        return  $result;
    }

    private function parseBracket($string)
    {
        $delimiters = [
            '(',
            ')'
        ];
        $replace = '|';
        
        $ready = str_replace($delimiters, $replace, $string);
        $result = explode($replace, $ready);
        $res = [];

        foreach ($result as $key => $value) {
            if (!empty($value)) {
                $res[] = trim($value);
            }
        }

        return  $res;
    }

    private function parseAnd($string)
    {
        $find = 'and';
        $isKeyWord = strpos($string, $find);

        if ($isKeyWord === false) {
            return $string;
        } else {
            $result = explode($find, $string);

            $res = [];
            foreach ($result as $key => $value) {
                if (!empty($value)) {
                    $res[] = trim($value);
                }
            }

            if (count($res) == 0) {
                return null;
            }
        }

        return  $res;
    }

    private function parseOr($string)
    {
        $find = 'or';
        $isKeyWord = strpos($string, $find);
        

        if ($isKeyWord === false) {
            $cond = $this->parseOperators(trim($string));
            if (isset($cond['key']) && isset($cond['value'])) {
                $k = $cond['key'];
                $v = $cond['value'];

                return [$k => $v];
            }
        } else {
            $res = [];
            $result = explode($find, $string);
            foreach ($result as $key => $value) {
                $cond = $this->parseOperators(trim($value));
                if (isset($cond['key']) && isset($cond['value'])) {
                    $k = $cond['key'];
                    $v = $cond['value'];
                    $res[$k] = $v;
                }
            }
            return  [
                'OR' => $res
            ];
        }

        return null;
    }

    private function parseOperators($condition)
    {
        // contrain.
        $result = explode(' contrain ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' LIKE';
            $value = '%' . trim($result[1]) . '%';

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // equals and lower than.
        $result = explode(' <= ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' <=';
            $value = $result[1];

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // equals and greater than.
        $result = explode(' >= ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' >=';
            $value = $result[1];

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // equals.
        $result = explode(' = ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' =';
            $value = $result[1];

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // greater than.
        $result = explode(' > ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' >';
            $value = $result[1];

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // lower than.
        $result = explode(' < ', $condition, 2);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' <';
            $value = $result[1];

            return [
                'key' => $key,
                'value' => $value
            ];
        }

        // spacing.
        $result = explode(' ', $condition, 3);
        if (count($result) > 1) {
            $key = trim($result[0]) . ' ' . $result[1];
            $value = $result[2];

            return [
                'key' => $key,
                'value' => $value
            ];
        }
    }

    private function validateFields($condition, &$validate)
    {
        // fields of the images table.
        $fields = [
            'id' => 'id',
            'imgw' => 'imgw',
            'imgh' => 'imgh',
            'imgt' => 'imgt',
            'imgl' => 'imgl',
            'filename' => 'filename'
        ];

        $operators = [
            'like' => 'like',
            '<=' => '<=',
            '>=' => '>=',
            '=' => '=',
            '<' => '<',
            '>' => '>'
        ];

        foreach ($condition as $key => $value) {
            if (is_array($value)) {
                $this->validateFields($value, $validate);
            } else {
                // nothing.
                $arr = explode(' ', $key);
                $fieldName = strtolower($arr[0]);
                $operator = strtolower($arr[1]);

                // fields validate.
                if (!isset($fields[$fieldName])) {
                    $validate['fields'] = [
                        $fieldName => $fieldName
                    ];
                }

                // operators validate.
                if (!isset($operators[$operator])) {
                    $validate['operator'] = [
                        $operator => $fieldName
                    ];
                }
            }
        }
    }

    /**
     * add a Image.
     *
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add()
    {
        $image = new ImageForm();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            if ($image->execute($formData)) {
                $model = TableRegistry::get('Images');
                $entity = $model->newEntity();

                $dropInfo = [
                    'imgw' => $formData['imgw'],
                    'imgh' => $formData['imgh'],
                    'imgl' => $formData['imgl'],
                    'imgt' => $formData['imgt']
                ];

                // upload image.
                $imageInfo = $this->uploadFile($formData['filename']);

                // drop image.
                $this->dropImage($imageInfo, $dropInfo);


                $entity->imgw = $formData['imgw'];
                $entity->imgh = $formData['imgh'];
                $entity->imgl = $formData['imgl'];
                $entity->imgt = $formData['imgt'];
                $entity->filename = (isset($imageInfo['name'])) ? $imageInfo['name'] : null;
                $entity->realName = (isset($imageInfo['realName'])) ? $imageInfo['realName'] : null;
                $entity->ext = (isset($imageInfo['ext'])) ? $imageInfo['ext'] : null;

                if ($model->save($entity)) {
                    $this->Flash->success('A image saved to database. the id is: ' . $entity->id);
                } else {
                    unlink($imageInfo['realName'] . DS . $imageInfo['ext']);
                }
            } else {
                $this->Flash->error('There was a problem submitting your form.');
            }
        }

        $this->set('data', $image);
        $this->render();
    }

    /**
     * check and save a image into the database.
     *
     * @param Array $uploadData Passed from $validate to this function containing our filename.
     * @return array Image information.
     */
    private function uploadFile($uploadData)
    {
        $res = [];

        // Basic checks
        if ($uploadData['size'] == 0 || $uploadData['error'] !== 0) {
            return false;
        }

        // Upload folder and path
        $uploadFolder = 'img'. DS .'images';
        $info = getimagesize($uploadData['tmp_name']);
        $ext = image_type_to_extension($info[2]);

        $time = time();
        $fileName = $time . $ext;
        $uploadPath =  $uploadFolder . DS . $fileName;

        // Make the dir if does not exist
        if (!file_exists($uploadFolder)) {
            mkdir($uploadFolder);
        }

        // Finally move from tmp to final location
        if (move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
            $res = $uploadData;
            $res['realName'] = $time;
            $res['ext'] = substr($ext, 1);
            $res['path'] = $uploadFolder;
        }

        return $res;
    }

    private function dropImage($imageInfo, $dropInfo)
    {
        // SETTINGS
        $isDrop = false;
        $newW = $dropInfo['imgw'];
        $newH = $dropInfo['imgh'];
        $left = $dropInfo['imgl'];
        $top = $dropInfo['imgt'];
        $path = $imageInfo['path'];
        $name = $imageInfo['realName'];
        $ext = $imageInfo['ext'];
        $quality = 100;
        $filePath = $path . DS . $name . '.' . $ext;
        // END OF SETTINGS

        if (preg_match('/jpg|jpeg/', $ext)) {
            $imgSrc = imagecreatefromjpeg($filePath);
            $ext = 'jpg';
            $newFilePath = $path . DS . $name . '.' . $ext;
        } elseif (preg_match('/png/', $ext)) {
            $imgSrc = imagecreatefrompng($filePath);
            $ext = 'png';
            $newFilePath = $path . DS . $name . '.' . $ext;
        }

        $srcW = imageSX($imgSrc);  // Width of the original image
        $srcH = imageSY($imgSrc);  // Height of the original image
        $minSide = min($srcW, $srcH);
       
        /*********** If you need this part uncomment it
        $ratio = $minSide / $thumb_width;
        $new_width = floor($srcW / $ratio);
        $new_height = floor($srcH / $ratio);
        **********************************************/

        $imgDest = imagecreatetruecolor($newW, $newH); //destination image.
        imagecopyresampled($imgDest, $imgSrc, 0, 0, $left, $top, $newW, $newH, $minSide, $minSide);

        switch ($ext) {
            case 'jpg':
                imagejpeg($imgDest, $newFilePath, $quality);
                $isDrop = true;
                break;
            case 'png':
                imagepng($imgDest, $newFilePath, $quality);
                $isDrop = true;
                break;
        }

        if ($isDrop === true) {
            unlink($filePath);
        }
        imagedestroy($imgSrc);
        imagedestroy($imgDest);
    }
}
