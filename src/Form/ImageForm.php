<?php
// in src/Form/ImageForm.php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ImageForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('imgw', 'integer')
            ->addField('imgh', 'integer')
            ->addField('imgl', 'integer')
            ->addField('imgt', 'integer')
            ->addField('filename', 'string');
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->add('imgw', [
                'validValue' => [
                    'rule' => ['range', 1, 5000],
                    'message' => 'the min of the width is 1.',
                ]
            ])
            ->add('imgh', [
                'validValue' => [
                    'rule' => ['range', 1, 5000],
                    'message' => 'the min of the height is 1.',
                ]
            ])
            ->add('imgl', [
                'validValue' => [
                    'rule' => ['range', 0, 1000],
                    'message' => 'a value is from 0 to 1000',
                ]
            ])
            ->add('imgt', [
                'validValue' => [
                    'rule' => ['range', 0, 1000],
                    'message' => 'a value is from 0 to 1000',
                ]
            ])
            ->add('filename', [
                'extension' => [
                    'rule' => [
                        'extension', ['jpeg', 'png', 'jpg']
                    ],
                    'message' => 'Only jpeg, png, jpg files'
                ],
                'filesize' => [
                    'rule' => [
                        'filesize', '<=', '3MB'
                    ],
                    'message' => 'Image size has to lower 3MB'
                ],
            ]);
    }

    // public function execute(array $data)
    // {
    //     // Send an email.
    //     return true;
    // }
}
