<?php
//echo $this->Html->script('jquery-ui-1.10.4.custom/js/jquery-1.10.2');

$this->assign('title', 'Image additional');

echo $this->Form->create($data, [
    // 'url' => array('controller' => 'Users','action' =>'newUser'),
    // 'class'=> 'classname',
    'enctype'=>'multipart/form-data'
]);

echo $this->Form->control('imgw');
echo $this->Form->control('imgh');
echo $this->Form->control('imgl');
echo $this->Form->control('imgt');
echo $this->Form->control('filename', array('label' => 'File name', 'type' => 'file'));

echo $this->Form->button('Submit');
echo $this->Form->end();
?>

<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#filename").change(function(){
    readURL(this);
});

</script>

<form id="form1" runat="server">
    <img id="blah" src="#" alt="your image" />
</form>