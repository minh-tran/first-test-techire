<?php
//echo $this->Html->script('images');
?>

<div class="wrapper">
    <div class="table">
        <div class="row header blue">
            <div class="cell">No.</div>
            <div class="cell">Fileinput</div>
            <div class="cell">Image width</div>
            <div class="cell">Image height</div>
            <div class="cell">Left</div>
            <div class="cell">Top</div>
        </div>
        
        <?php foreach ($data as $item): ?>
        <div class="row">
            <div class="cell">
                No.
            </div>
            <div class="cell">
                <?= $item->filename; ?>
            </div>
            <div class="cell">
                <?= $item->imgw; ?>
            </div>
            <div class="cell">
                <?= $item->imgh; ?>
            </div>
            <div class="cell">
                <?= $item->imgl; ?>
            </div>
            <div class="cell">
                <?= $item->imgt; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<ul class="pagination right">
    <?php
    // Pagination urls
        echo $this->Paginator->first('<< ' . __('first'));
        echo $this->Paginator->prev('< ' . __('previous'));
        echo $this->Paginator->numbers();
        echo $this->Paginator->next(__('next') . ' >');
        echo $this->Paginator->last(__('last') . ' >>');
    ?>
</ul>

<?php
echo $this->Form->create();
echo $this->Form->textarea('filter', [
    'id' => 'img-filter',
    'value' => $filter
]);
echo $this->Form->button('Submit', ['type' => 'button', 'id' => 'btn-filter']);
echo $this->Form->end();
?>
